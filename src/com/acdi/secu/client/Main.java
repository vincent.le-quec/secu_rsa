package com.acdi.secu.client;

import com.acdi.secu.common.*;
import com.acdi.secu.model.EncryptionMethod;
import com.acdi.secu.model.KeyPair;
import com.acdi.secu.model.PublicKey;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Main {
    private static final String defaultIp = "127.0.0.1";
    private static final int defaultPort = 4242;

    public static void main(String[] args) {
        String ip = defaultIp;
        int port = defaultPort;
        String name;
        EncryptionMethod em = EncryptionMethod.SIMPLE_ENCRYPTION;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Use default settings [y/n]: (" + ip + ":" + port + " with simple encryption)\n$> ");
            String str = br.readLine();
            if (str.equals("n") || str.equals("N")) {
                System.out.print("Client IP\n$> ");
                ip = br.readLine();
                System.out.print("Client port\n$> ");
                port = Integer.parseInt(br.readLine());

                System.out.println("Choose your encryption:");
                System.out.println("1- SIMPLE_ENCRYPTION");
                System.out.println("2- SIGN_ENCRYPTION");
                System.out.println("3- HTTPS_ENCRYPTION");
                System.out.print("$> ");
                em = EncryptionMethod.getFromString(br.readLine());
            }

            System.out.print("Enter a pseudo\n$> ");
            name = br.readLine();

            // Generate keys
            System.out.println("Configuring chat ...");
            KeyPair kp = KeyPairUtils.generateKeyPair();

            Service s = new Service(ip, port);
            // Send encryption methode
            s.sendMessage(EncryptionMethod.toSimpleString(em));
            // Send public key
            s.sendMessage(kp.getN().toString() + " " + kp.getE().toString());

            // Get public key of new user
            String keys = s.getMessage();
            final String[] arr = keys.split(" ");
            final Cipher c = new Cipher(kp.getPrivateKey(), new PublicKey(new BigInteger(arr[0]), new BigInteger(arr[1])), em);

            // Send name
            s.sendMessage(c.cipher(name));
            // Get name
            String msg = s.getMessage();
            String hostName = c.decipher(msg);

            // Thread message
            System.out.println("Configuration done and connected to server.");
            System.out.println("Off you go ;)");
            MessageReceiver mr = new MessageReceiver(hostName, s, c);
            MessageSender ms = new MessageSender(s, mr, c);
            mr.start();
            ms.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
