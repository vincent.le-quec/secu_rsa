package com.acdi.secu.server;

import java.net.*;

public class Main {

    public static void main(String[] args) {
        ServerSocket ecoute;
        Socket so;
        int port = 4242;

        try {
            ecoute = new ServerSocket(port);
            System.out.println("[INFO] Server running on port " + port);

            while (true) {
                so = ecoute.accept(); // Bloquant
                System.out.println(so.toString());
                new Server(so).start();
            }

        } catch (BindException e) {
            System.out.println("[ERROR] Impossible d’attacher localement le socket:" + e.toString());
        } catch (ConnectException e) {
            System.out.println("[ERROR] Le serveur distant refuse la connexion (pas de serveur rattaché au port, ...):" + e.toString());
        } catch (NoRouteToHostException e) {
            System.out.println("[ERROR] Problème pour joindre la machine distante (firewall, routage, ...):" + e.toString());
        } catch (SocketException e) {
            System.out.println("[ERROR] Problème au niveau TCP:" + e.toString());
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.toString());
        }
    }
}
