package com.acdi.secu.server;

import com.acdi.secu.common.*;
import com.acdi.secu.model.EncryptionMethod;
import com.acdi.secu.model.KeyPair;
import com.acdi.secu.model.PublicKey;

import java.io.*;
import java.math.BigInteger;
import java.net.*;

public class Server extends Thread {
    private final static String name = "Server";
    private Socket socket;
    private BufferedReader br;
    private PrintWriter os;

    public Server(Socket socket) {
        System.out.println("[Server::Server]");
        this.socket = socket;
    }

    public void run() {
        try {
            this.os = new PrintWriter(this.socket.getOutputStream(), true);
            this.br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            // Generate keys
            KeyPair kp = KeyPairUtils.generateKeyPair();

            Service s = new Service(this.os, this.br);

            // Get encryption methode
            EncryptionMethod em = EncryptionMethod.getFromString(s.getMessage());
            // Get public key of new user
            String keys = s.getMessage();
            final String[] arr = keys.split(" ");
            final Cipher c = new Cipher(kp.getPrivateKey(), new PublicKey(new BigInteger(arr[0]), new BigInteger(arr[1])), em);

            // Send public key
            s.sendMessage(kp.getN().toString() + " " + kp.getE().toString());

            // Get name
            String msg = s.getMessage();
            String clientName = c.decipher(msg);
            // DEBUG
            System.out.println("Incoming connection of '" + clientName + "' with '" + EncryptionMethod.toReadableString(em) + "'");
            // Send name
            s.sendMessage(c.cipher(name));

            // Thread message
            MessageReceiver mr = new MessageReceiver(clientName, s, c);
            MessageSender ms = new MessageSender(s, mr, c);
            mr.start();
            ms.start();
        } catch (ConnectException e) {
            System.out.println("[Server::run][ERROR] Le serveur distant refuse la connexion (pas de serveur rattaché au port, ...):" + e.toString());
        } catch (NoRouteToHostException e) {
            System.out.println("[Server::run][ERROR] Problème pour joindre la machine distante (firewall, routage, ...):" + e.toString());
        } catch (SocketException e) {
            System.out.println("[Server::run][ERROR] Problème au niveau TCP:" + e.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
