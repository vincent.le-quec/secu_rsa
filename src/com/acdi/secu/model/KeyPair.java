package com.acdi.secu.model;

import com.acdi.secu.common.Cipher;

import java.math.BigInteger;

public class KeyPair {

    private BigInteger p, q, n, m, e, u;

    public KeyPair() {
    }

    public KeyPair(BigInteger p, BigInteger q, BigInteger n, BigInteger m, BigInteger e) {
        this.p = p;
        this.q = q;
        this.n = n;
        this.m = m;
        this.e = e;
    }

    public BigInteger getP() {
        return p;
    }

    public void setP(BigInteger p) {
        this.p = p;
    }

    public BigInteger getQ() {
        return q;
    }

    public void setQ(BigInteger q) {
        this.q = q;
    }

    public BigInteger getN() {
        return n;
    }

    public void setN(BigInteger n) {
        this.n = n;
    }

    public BigInteger getM() {
        return m;
    }

    public void setM(BigInteger m) {
        this.m = m;
    }

    public BigInteger getE() {
        return e;
    }

    public void setE(BigInteger e) {
        this.e = e;
    }

    public BigInteger getU() {
        return u;
    }

    public void setU(BigInteger u) {
        this.u = u;
    }

    public PrivateKey getPrivateKey() {
        return new PrivateKey(n, u);
    }

    public PublicKey getPublicKey() {
        return new PublicKey(n, e);
    }

    public Cipher toCipher() {
        return new Cipher(getPrivateKey(), getPublicKey());
    }

    @Override
    public String toString() {
        return "KeyPair{" +
                "p=" + p +
                ", q=" + q +
                ", n=" + n +
                ", m=" + m +
                ", e=" + e +
                '}';
    }
}
