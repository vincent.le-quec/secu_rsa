package com.acdi.secu.model;

import java.math.BigInteger;

public class PrivateKey {

    private final BigInteger n, u;

    public PrivateKey(BigInteger n, BigInteger u) {
        this.n = n;
        this.u = u;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getU() {
        return u;
    }

    @Override
    public String toString() {
        return "PrivateKey{" +
                "n=" + n +
                ", u=" + u +
                '}';
    }
}
