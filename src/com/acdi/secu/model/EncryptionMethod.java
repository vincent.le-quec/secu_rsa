package com.acdi.secu.model;

public enum EncryptionMethod {
    SIMPLE_ENCRYPTION,
    SIGN_ENCRYPTION,
    HTTPS_ENCRYPTION;

    public static EncryptionMethod getFromString(final String v) {
        switch (v) {
            case "2": return EncryptionMethod.SIGN_ENCRYPTION;
            case "3": return EncryptionMethod.HTTPS_ENCRYPTION;
            default: return EncryptionMethod.SIMPLE_ENCRYPTION;
        }
    }

    public static String toSimpleString(final EncryptionMethod v) {
        switch (v) {
            case SIGN_ENCRYPTION: return "2";
            case HTTPS_ENCRYPTION: return "3";
            default: return "1";
        }
    }

    public static String toReadableString(final EncryptionMethod v) {
        switch (v) {
            case SIGN_ENCRYPTION: return "Sign encryption";
            case HTTPS_ENCRYPTION: return "HTTPS encryption";
            default: return "Simple encryption";
        }
    }
}
