package com.acdi.secu.model;

import java.math.BigInteger;

public class PublicKey {

    private final BigInteger n, e;

    public PublicKey(BigInteger n, BigInteger e) {
        this.n = n;
        this.e = e;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getE() {
        return e;
    }

    @Override
    public String toString() {
        return "PublicKey{" +
                ", n=" + n +
                ", e=" + e +
                '}';
    }
}
