package com.acdi.secu.test;

import com.acdi.secu.common.Cipher;
import com.acdi.secu.common.KeyPairUtils;
import com.acdi.secu.model.EncryptionMethod;
import com.acdi.secu.model.PrivateKey;
import com.acdi.secu.model.PublicKey;

import java.math.BigInteger;

public class Test {

    public static void main(String[] args) {
        PrivateKey priv = new PrivateKey(new BigInteger("5141"), new BigInteger("4279"));
        PublicKey pub = new PublicKey(new BigInteger("5141"), new BigInteger("7"));
        Cipher c = new Cipher(priv, pub);

        String plain = "Bonjour !";
        String encrypted = "386 1858 2127 2809 1858 1774 737 3675 244";


        // verify with example
        long startTime = System.currentTimeMillis();
        assertEquals("Verify crypted string : ", startTime, encrypted, c.cipher(plain));
        startTime = System.currentTimeMillis();
        assertEquals("Verify plain string : ", startTime, plain, c.decipher(encrypted));


        // verify simple encryption
        c = KeyPairUtils.generateKeyPair().toCipher();
        startTime = System.currentTimeMillis();
        assertEquals("Verify both ways : ", startTime, plain, c.decipher(c.cipher(plain)));


        // verify exceptions
        startTime = System.currentTimeMillis();
        boolean testIsPassed = false;
        final String str = "Verify decryption error : ";
        try {
            c.decipher("11111 11111 11111 11111");
        } catch(IllegalArgumentException iae) {
            assertEquals(str, startTime, iae.getMessage(), "Decryption failed, chat is corrupted");
            testIsPassed = true;
        }
        if(!testIsPassed) System.out.println(str + "TEST FAILED (" + (System.currentTimeMillis() - startTime) + "ms)");


        // test signing
        c.setEncryptionMethod(EncryptionMethod.SIGN_ENCRYPTION);
        startTime = System.currentTimeMillis();
        assertEquals("Verify sign both ways : ", startTime, plain, c.decipher(c.cipher(plain)));


        // test https
        c.setEncryptionMethod(EncryptionMethod.HTTPS_ENCRYPTION);
        startTime = System.currentTimeMillis();
        assertEquals("Verify symmetric encryption : ", startTime, plain, c.decipher(c.cipher(plain)));


        // select encryption method
        assertEquals("", 0, EncryptionMethod.SIMPLE_ENCRYPTION.toString(), EncryptionMethod.getFromString("1").toString());
        assertEquals("", 0, EncryptionMethod.SIGN_ENCRYPTION.toString(), EncryptionMethod.getFromString("2").toString());
        assertEquals("", 0, EncryptionMethod.HTTPS_ENCRYPTION.toString(), EncryptionMethod.getFromString("3").toString());
        assertEquals("", 0, EncryptionMethod.SIMPLE_ENCRYPTION.toString(), EncryptionMethod.getFromString("4").toString());
    }

    private static void assertEquals(final String str, final long startTime, final String s1, final String s2) {
        if(s1.equals(s2)) {
            System.out.println(str + "TEST OK" + (startTime != 0 ? " (" + (System.currentTimeMillis() - startTime) + "ms)" : ""));
        } else {
            System.out.println(str + "TEST FAILED" + (startTime != 0 ? " (" + (System.currentTimeMillis() - startTime) + "ms)" : ""));
        }
    }
}
