package com.acdi.secu.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Service {
    private String ip;
    private int port;

    private Socket so;
    private PrintWriter os;
    private BufferedReader br;

    public Service(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.init();
    }

    public Service(Socket so) {
        this.so = so;
        this.init();
    }

    public Service(PrintWriter os, BufferedReader br) {
        this.os = os;
        this.br = br;
    }

    public void init() {
        try {
            if (this.so == null) {
                this.so = new Socket(ip, port);
            }
            this.os = new PrintWriter(so.getOutputStream(), true);
            this.br = new BufferedReader(new InputStreamReader(so.getInputStream()));
        } catch (UnknownHostException e) {
            System.out.println("[ERROR] " + e.toString());
            System.exit(1);
        } catch (IOException e) {
            System.out.println("[ERROR] Aucun serveur n’est rattaché au port: " + e.toString());
            System.exit(1);
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.toString());
            System.exit(1);
        }
    }

    public void sendMessage(String str) {
        this.os.println(str);
        this.os.flush();
    }

    public String getMessage() throws IOException {
        return this.br.readLine();
    }

    public void close() throws IOException {
        if (this.os != null)
            this.os.close();
        if (this.br != null)
            this.br.close();
        if (this.so != null && !this.so.isClosed())
            this.so.close();
    }
}
