package com.acdi.secu.common;

import java.io.IOException;

public class MessageReceiver extends Thread {
    private final String sender;
    private final Service service;
    private final Cipher cipher;


    public MessageReceiver(String sender, Service service, Cipher cipher) {
        this.sender = sender;
        this.service = service;
        this.cipher = cipher;
    }

    @Override
    public void run() {
        super.run();
        boolean run = true;

        while (run) {
            try {
                String msg = this.service.getMessage();
                if (msg != null && !msg.isEmpty()) {
                    String decrypted = cipher.decipher(msg);
                    if (decrypted.equals("exit"))
                        break;
                    System.out.print("\n[" + this.sender + "] " + decrypted + "\n$> ");
                }
            } catch (IOException e) {
                run = false;
            }
        }
        try {
            this.service.close();
            // TODO: find a better way
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
