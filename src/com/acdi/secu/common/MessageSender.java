package com.acdi.secu.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MessageSender extends Thread {
    private final Service service;
    private Boolean exit = false;
    private final MessageReceiver mr;
    private final Cipher cipher;

    public MessageSender(Service service, MessageReceiver mr, Cipher cipher) {
        this.service = service;
        this.mr = mr;
        this.cipher = cipher;
    }

    @Override
    public void run() {
        super.run();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (!this.exit) {
            try {
                System.out.print("$> ");
                String str = br.readLine();
                if (!str.isEmpty()) {
                    this.checkInput(str);
                }
            } catch (IOException e) {
                this.exit = true;
            }
        }
        try {
            this.mr.interrupt();
            this.service.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkInput(String str) {
        switch (str) {
            case "exit":
                this.service.sendMessage(cipher.cipher(str));
                this.exit = true;
                break;
            default:
                this.service.sendMessage(cipher.cipher(str));
                break;
        }
    }
}
