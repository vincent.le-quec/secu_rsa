package com.acdi.secu.common;

import com.acdi.secu.model.KeyPair;

import java.math.BigInteger;
import java.security.SecureRandom;

public class KeyPairUtils {

    private static final int BIG_INTEGER_LENGTH = 512;

    public static KeyPair generateKeyPair() {
        final KeyPair keys = generatePublicKey();
        generatePrivateKey(keys);
        return keys;
    }

    private static void generatePrivateKey(final KeyPair keys) {
//        System.out.println("generatePrivateKey : mod");
        BigInteger u = keys.getE().modInverse(keys.getM());
        keys.setU(u);
    }

    private static KeyPair generatePublicKey() {
        BigInteger p = getRandomPrimeNumber();

        // find q that is different from p
        BigInteger q = null;
//        System.out.println("generatePublicKey : while q");
        while (q == null || q.equals(p)) {
            q = getRandomPrimeNumber();
        }

        BigInteger n = p.multiply(q);
        BigInteger m = p.subtract(BigInteger.ONE)
                .multiply(q.subtract(BigInteger.ONE)); // indicatrice d'Euler

        // find e that must be odd
        BigInteger e = null;
//        System.out.println("generatePublicKey : while e");
        while(e == null || e.mod(new BigInteger("2")).equals(BigInteger.ZERO) || !e.gcd(m).equals(BigInteger.ONE)) {
            e = getRandomNumber();
        }

        return new KeyPair(p, q, n, m, e);
    }

    private static BigInteger getRandomPrimeNumber() {
        return BigInteger.probablePrime(BIG_INTEGER_LENGTH, new SecureRandom());
    }

    private static BigInteger getRandomNumber() {
        return BigInteger.probablePrime(BIG_INTEGER_LENGTH, new SecureRandom());
    }
}
