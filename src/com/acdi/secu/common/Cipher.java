package com.acdi.secu.common;

import com.acdi.secu.model.EncryptionMethod;
import com.acdi.secu.model.PrivateKey;
import com.acdi.secu.model.PublicKey;
import com.acdi.secu.model.SymetricKey;

import java.math.BigInteger;

public class Cipher {

    private static final BigInteger MAX_VALUE = new BigInteger("127");
    private static final BigInteger MIN_VALUE = BigInteger.ZERO;

    private final PrivateKey privateKey;
    private final PublicKey publicKey;
    private SymetricKey symetricKey;
    private EncryptionMethod encryptionMethod = EncryptionMethod.SIMPLE_ENCRYPTION;

    public Cipher(PrivateKey privateKey, PublicKey publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public Cipher(PrivateKey privateKey, PublicKey publicKey, EncryptionMethod encryptionMethod) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.encryptionMethod = encryptionMethod;
    }

    public Cipher(PrivateKey privateKey, PublicKey publicKey, EncryptionMethod encryptionMethod, SymetricKey symetricKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.symetricKey = symetricKey;
        this.encryptionMethod = encryptionMethod;
    }

    public String cipher(final String str) {
        switch (encryptionMethod) {
            case SIMPLE_ENCRYPTION:
                return this.encrypt(str, publicKey.getE(), publicKey.getN());
            case SIGN_ENCRYPTION:
                final String cipher = this.encrypt(str, publicKey.getE(), publicKey.getN());
                return this.encrypt(cipher, privateKey.getU(), privateKey.getN());
            default:
                return str;
        }
    }

    public String decipher(final String str) {
//        System.out.println("\nReceived: " + str);
        switch (encryptionMethod) {
            case SIMPLE_ENCRYPTION:
                return this.decrypt(str, privateKey.getU(), privateKey.getN(), true);
            case SIGN_ENCRYPTION:
                final String unsign = this.decrypt(str, publicKey.getE(), publicKey.getN());
                return this.decrypt(unsign, privateKey.getU(), privateKey.getN(), true);
            default:
                return str;
        }
    }

    private String encrypt(String str, BigInteger a, BigInteger b) {
        final StringBuilder res = new StringBuilder();

        for(char c : str.toCharArray()) {
            final BigInteger i = new BigInteger("" + (int)c);
            final BigInteger bg = i.modPow(a, b);
            res.append(bg.toString())
                    .append(" ");
        }
        return res.substring(0, res.length() - 1);
    }

    private String decrypt(String str, BigInteger a, BigInteger b) {
        return this.decrypt(str, a, b, false);
    }

    private String decrypt(String str, BigInteger a, BigInteger b, boolean verify) {
        final String[] arr = str.split(" ");
        final StringBuilder res = new StringBuilder();

        for(final String s : arr) {
            final BigInteger bg = new BigInteger(s);
            final BigInteger i = bg.modPow(a, b);

            // verify if value is correct
            if(verify && (i.compareTo(MAX_VALUE) > 0 || i.compareTo(MIN_VALUE) < 0))
                throw new IllegalArgumentException("Decryption failed, chat is corrupted");

            res.append((char)Integer.parseInt(i.toString()));
        }
        return res.toString();
    }

    public void setEncryptionMethod(EncryptionMethod encryptionMethod) {
        this.encryptionMethod = encryptionMethod;
    }
}
