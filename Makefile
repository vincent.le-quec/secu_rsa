SHELL:=/bin/bash

#https://www.codejava.net/java-core/tools/using-javac-command
#javac -classpath mail.jar EmailSender.java
all: build serve

build: clean
	javac -d bin src/com/acdi/secu/**/*.java

cli: build
	java -classpath bin com.acdi.secu.client.Main

serve: build
	java -classpath bin com.acdi.secu.server.Main

test: build
	java -classpath bin com.acdi.secu.test.Test

clean:
	rm -rf bin/*