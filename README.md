# Secu_RSA

## build and run server
```sh
make
```
It builds the app in the bin folder and then runs the server.

## build applications
```sh
make build
```
It builds the app in the bin folder.

## run client
```sh
make serve
```
It runs the server application.

## run client
```sh
make cli
```
It runs the client application.

## run tests
```sh
make test
```
It runs the application tests.

## delete built applications
```sh
make clean
```
It deletes every built files in the bin folder.